#!/usr/bin/python
from PyQt4.QtGui import *
from PyQt4.QtCore import *
from PyQt4 import QtCore, QtGui
from PyQt4.QtCore import QCoreApplication, Qt
import qt4reactor
import myDesktopServerProtocol as serverProtocol
import sys
import os
import socket

app = QApplication(sys.argv)
widget = QWidget()
qt4reactor.install()

platform = sys.platform[:3]

if platform == 'lin':
    from Xlib.display import Display
    from Xlib import X
    from Xlib.ext.xtest import fake_input

elif platform == 'mac':
    from Quartz import *
    from AppKit import NSEvent

    pressID = [None, kCGEventLeftMothe,
               kCGEventRightMouseDown, kCGEventOtherMouseDown]
    releaseID = [None, kCGEventLeftMouseUp,
                 kCGEventRightMouseUp, kCGEventOtherMouseUp]

elif platform == 'win':
    from ctypes import *
    import ctypes
    from ctypes import wintypes
    import win32api, win32con, win32gui

    user32 = ctypes.WinDLL('user32', use_last_error=True)

    wintypes.ULONG_PTR = wintypes.WPARAM


    class MOUSEINPUT(ctypes.Structure):
        _fields_ = (("dx", wintypes.LONG),
                    ("dy", wintypes.LONG),
                    ("mouseData", wintypes.DWORD),
                    ("dwFlags", wintypes.DWORD),
                    ("time", wintypes.DWORD),
                    ("dwExtraInfo", wintypes.ULONG_PTR))


    class KEYBDINPUT(ctypes.Structure):
        _fields_ = (("wVk", wintypes.WORD),
                    ("wScan", wintypes.WORD),
                    ("dwFlags", wintypes.DWORD),
                    ("time", wintypes.DWORD),
                    ("dwExtraInfo", wintypes.ULONG_PTR))

        def __init__(self, *args, **kwds):
            super(KEYBDINPUT, self).__init__(*args, **kwds)
            # some programs use the scan code even if KEYEVENTF_SCANCODE
            # isn't set in dwFflags, so attempt to map the correct code.
            if not self.dwFlags & KEYEVENTF_UNICODE:
                self.wScan = user32.MapVirtualKeyExW(self.wVk,
                                                     MAPVK_VK_TO_VSC, 0)


    class HARDWAREINPUT(ctypes.Structure):
        _fields_ = (("uMsg", wintypes.DWORD),
                    ("wParamL", wintypes.WORD),
                    ("wParamH", wintypes.WORD))


    class INPUT(ctypes.Structure):
        class _INPUT(ctypes.Union):
            _fields_ = (("ki", KEYBDINPUT),
                        ("mi", MOUSEINPUT),
                        ("hi", HARDWAREINPUT))

        _anonymous_ = ("_input",)
        _fields_ = (("type", wintypes.DWORD),
                    ("_input", _INPUT))


    LPINPUT = ctypes.POINTER(INPUT)


    def _check_count(result, func, args):
        if result == 0:
            raise ctypes.WinError(ctypes.get_last_error())
        return args


    user32.SendInput.errcheck = _check_count
    user32.SendInput.argtypes = (wintypes.UINT,  # nInputs
                                 LPINPUT,  # pInputs
                                 ctypes.c_int)  # cbSize

    class POINT(Structure):
        _fields_ = [("x", c_ulong),
                    ("y", c_ulong)]

button_ids = [None, 1, 3, 2, 4, 5, 6, 7]

KEY_BackSpace = 0xff08
KEY_Tab = 0xff09
KEY_Return = 0xff0d
KEY_Escape = 0xff1b
KEY_Insert = 0xff63
KEY_Delete = 0xffff
KEY_Home = 0xff50
KEY_End = 0xff57
KEY_PageUp = 0xff55
KEY_PageDown = 0xff56
KEY_Left = 0xff51
KEY_Up = 0xff52
KEY_Right = 0xff53
KEY_Down = 0xff54
KEY_F1 = 0xffbe
KEY_F2 = 0xffbf
KEY_F3 = 0xffc0
KEY_F4 = 0xffc1
KEY_F5 = 0xffc2
KEY_F6 = 0xffc3
KEY_F7 = 0xffc4
KEY_F8 = 0xffc5
KEY_F9 = 0xffc6
KEY_F10 = 0xffc7
KEY_F11 = 0xffc8
KEY_F12 = 0xffc9
KEY_F13 = 0xFFCA
KEY_F14 = 0xFFCB
KEY_F15 = 0xFFCC
KEY_F16 = 0xFFCD
KEY_F17 = 0xFFCE
KEY_F18 = 0xFFCF
KEY_F19 = 0xFFD0
KEY_F20 = 0xFFD1
KEY_ShiftLeft = 0xffe1
KEY_ShiftRight = 0xffe2
KEY_ControlLeft = 0xffe3
KEY_ControlRight = 0xffe4
KEY_MetaLeft = 0xffe7
KEY_MetaRight = 0xffe8
KEY_AltLeft = 0xffe9
KEY_AltRight = 0xffea

KEY_Scroll_Lock = 0xFF14
KEY_Sys_Req = 0xFF15
KEY_Num_Lock = 0xFF7F
KEY_Caps_Lock = 0xFFE5
KEY_Pause = 0xFF13
KEY_Super_L = 0xFFEB
KEY_Super_R = 0xFFEC
KEY_Hyper_L = 0xFFED
KEY_Hyper_R = 0xFFEE

INPUT_MOUSE = 0
INPUT_KEYBOARD = 1
INPUT_HARDWARE = 2

KEYEVENTF_EXTENDEDKEY = 0x0001
KEYEVENTF_KEYUP = 0x0002
KEYEVENTF_UNICODE = 0x0004
KEYEVENTF_SCANCODE = 0x0008

MAPVK_VK_TO_VSC = 0

# List of all codes for keys:
# # msdn.microsoft.com/en-us/library/dd375731
UP = 0x26
DOWN = 0x28
A = 0x41

keymap = {
    16777219: KEY_BackSpace,
    16777217: KEY_Tab,
    16777220: KEY_Return,
    16777216: KEY_Escape,
    16777222: KEY_Insert,
    16777223: KEY_Delete,
    16777232: KEY_Home,
    16777233: KEY_End,
    16777238: KEY_PageUp,
    16777239: KEY_PageDown,
    16777234: KEY_Left,
    16777235: KEY_Up,
    16777236: KEY_Right,
    16777237: KEY_Down,
    16777264: KEY_F1,
    16777265: KEY_F2,
    16777266: KEY_F3,
    16777267: KEY_F4,
    16777268: KEY_F5,
    16777269: KEY_F6,
    16777270: KEY_F7,
    16777271: KEY_F8,
    16777272: KEY_F9,
    16777273: KEY_F10,
    16777274: KEY_F11,
    16777275: KEY_F12,
    16777276: KEY_F13,
    16777277: KEY_F14,
    16777278: KEY_F15,
    16777279: KEY_F16,
    16777280: KEY_F17,
    16777281: KEY_F18,
    16777282: KEY_F19,
    16777283: KEY_F20,

    16777254: KEY_Scroll_Lock,
    16777226: KEY_Sys_Req,
    16777253: KEY_Num_Lock,
    16777252: KEY_Caps_Lock,
    16777224: KEY_Pause,
    16777299: KEY_Super_L,
    16777300: KEY_Super_R,
    16777302: KEY_Hyper_L,
    16777303: KEY_Hyper_R}


# ---------#
## Linux ##
# ---------#
class x11_Mouse:
    def __init__(self):
        self.display = Display()

    def press(self, x, y, button=1):
        self.move(x, y)
        fake_input(self.display, X.ButtonPress, button_ids[button])
        self.display.sync()

    def release(self, x, y, button=1):
        self.move(x, y)
        fake_input(self.display, X.ButtonRelease, button_ids[button])
        self.display.sync()

    def move(self, x, y):
        # if (x, y) != self.position( ):
        fake_input(self.display, X.MotionNotify, x=x, y=y)
        self.display.sync()

    def position(self):
        pos = self.display.screen().root.query_pointer()._data
        return pos['root_x'], pos['root_y']

    def screen_size(self):
        width = self.display.screen().width_in_pixels
        height = self.display.screen().height_in_pixels
        return width, height


class x11_Keyboard:
    def __init__(self):
        self.display = Display()

    def press(self, key):
        if key >= 256:
            keycode = self.display.keysym_to_keycode(keymap[key])
        keycode = self.display.keysym_to_keycode(key)
        fake_input(self.window(), X.KeyPress, keycode)
        self.display.sync()

    def release(self, key):
        if key >= 256:
            keycode = self.display.keysym_to_keycode(keymap[key])
        keycode = self.display.keysym_to_keycode(key)
        fake_input(self.window(), X.KeyRelease, keycode)
        self.display.sync()

    def window(self):
        return self.display.get_input_focus()._data['focus']


# ------------#
## Mac OS X ##
# ------------#
class mac_Mouse:
    def press(self, x, y, button=1):
        event = CGEventCreateMouseEvent(None,
                                        pressID[button],
                                        (x, y),
                                        button - 1)
        CGEventPost(kCGHIDEventTap, event)

    def release(self, x, y, button=1):
        event = CGEventCreateMouseEvent(None,
                                        releaseID[button],
                                        (x, y),
                                        button - 1)
        CGEventPost(kCGHIDEventTap, event)

    def move(self, x, y):
        move = CGEventCreateMouseEvent(None, kCGEventMouseMoved, (x, y), 0)
        CGEventPost(kCGHIDEventTap, move)

    def drag(self, x, y):
        drag = CGEventCreateMouseEvent(None, kCGEventLeftMouseDragged, (x, y), 0)
        CGEventPost(kCGHIDEventTap, drag)

    def position(self):
        loc = NSEvent.mouseLocation()
        return loc.x, CGDisplayPixelsHigh(0) - loc.y

    def screen_size(self):
        return CGDisplayPixelsWide(0), CGDisplayPixelsHigh(0)


class mac_Keyboard:
    def press(self, key):
        pass

    def release(self, key):
        pass

    def window(self):
        pass


# -----------#
## Windows ##
# -----------#
class win_Mouse:
    def press(self, x, y, button=1):
        buttonAction = 2 ** ((2 * button) - 1)
        self.move(x, y)
        win32api.mouse_event(buttonAction, x, y)

    def release(self, x, y, button=1):
        buttonAction = 2 ** (2 * button)
        self.move(x, y)
        win32api.mouse_event(buttonAction, x, y)

    def move(self, x, y):
        windll.user32.SetCursorPos(x, y)

    def position(self):
        pt = POINT()
        windll.user32.GetCursorPos(byref(pt))
        return pt.x, pt.y

    def screen_size(self):
        width = windll.user32.GetSystemMetrics(0)
        height = windll.user32.GetSystemMetrics(1)
        return width, height


class win_Keyboard:
    def press(self, key):
        x = INPUT(type=INPUT_KEYBOARD,
                  ki=KEYBDINPUT(wVk=key))
        windll.user32.SendInput(1, ctypes.byref(x), ctypes.sizeof(x))

    def release(self, key):
        x = INPUT(type=INPUT_KEYBOARD,
                  ki=KEYBDINPUT(wVk=key,
                                dwFlags=KEYEVENTF_KEYUP))
        windll.user32.SendInput(1, ctypes.byref(x), ctypes.sizeof(x))

    def window(self):
        pass


# ----------------------------------------------------------------#
## inherit the appropriate category based on system type,       ##
## so just needed import Mouse and Keyboard in our application, ##
# ----------------------------------------------------------------#
if platform == 'lin':
    class Mouse(x11_Mouse):
        pass


    class Keyboard(x11_Keyboard):
        pass

elif platform == 'mac':
    class Mouse(mac_Mouse):
        pass


    class Keyboard(mac_Keyboard):
        pass

elif platform == 'win':
    class Mouse(win_Mouse):
        pass


    class Keyboard(win_Keyboard):
        pass


class rdcProtocol(serverProtocol.RDCServerProtocol):
    """
    this class is inheritance from RDCServerProtocol,
    the class be responsible for achieve some of functions
    include (
    making screen pixel,
    execute:
    mouse event,
    keyboard event,
    copy text,
    send cut text ) etc...
    """

    def __init__(self):
        serverProtocol.RDCServerProtocol.__init__(self)
        self._array = QByteArray()
        self._buffer = QBuffer(self._array)
        self._buffer.open(QIODevice.WriteOnly)
        self._clipboard = QApplication.clipboard()
        self._maxWidth = QApplication.desktop().size().width()
        self._maxHeight = QApplication.desktop().size().height()
        # self.keyboard = input_.Keyboard()
        self.keyboard = Keyboard()
        # self.mouse = input_.Mouse()
        self.mouse = Mouse()

    def handleKeyEvent(self, key, flag=None):
        '''
        if flag == 6:
            self.keyboard.press(key)
        else flag == 7:
            self.keyboard.release(key)
        '''
        self.keyboard.press(key)
        self.keyboard.release(key)

    def handleMouseEvent(self, x, y, buttonmask=0, flag=None):
        # print(x, y, buttonmask, flag)
        if flag == 5:  # move mouse event
            self.mouse.move(x, y)

        elif flag == 2:  # mouse button down
            self.mouse.press(x, y, buttonmask)

        elif flag == 3:  # mouse button up
            self.mouse.release(x, y, buttonmask)

        elif flag == 4:  # mouse button duble clicked
            self.mouse.press(x, y, buttonmask)
            self.mouse.release(x, y, buttonmask)

    def handleClientCopyText(self, text):
        """
        copy text from client, and then set the text in clipboard
        """
        self._clipboard.setText(text)

    def cutTextToClient(self):
        """
        cut text to client
        """
        text = self._clipboard.text()
        self.sendCutTextToClient(text)

    def _makeFramebuffer(self, width, height):
        pix = QPixmap.grabWindow(QApplication.desktop().winId())
        # hwnd = getHwnd.enumHandler()
        # win32gui.SetForegroundWindow(1180962)
        # pix = QPixmap.grabWindow(1180962)
        pix = pix.scaled(width, height)

        if width >= self._maxWidth or height >= self._maxHeight:
            width = self._maxWidth
            height = self._maxHeight

        pix.save(self._buffer, 'jpeg')
        pixData = self._buffer.data()

        self._array.clear()
        self._buffer.close()

        return "%s" % pixData


class RDCFactory(serverProtocol.RDCFactory):
    def __init__(self, password=None):
        serverProtocol.RDCFactory.__init__(self, password)
        self.protocol = rdcProtocol

    def buildProtocol(self, addr):
        return serverProtocol.RDCFactory.buildProtocol(self, addr)

    def readyConnection(self, server):
        self.server = server

    # -----------------------#


## myDesktopServer GUI ##
# -----------------------#
class RDCServerGUI(QDialog):
    """
    The PyRDCServerGUI responsible provide GUI interface, operate
    the PyRDCServer.
    """

    def __init__(self, reactor, parent=None):
        super(RDCServerGUI, self).__init__(parent)
        self.setupUI()

        mainLayout = QGridLayout()
        mainLayout.addWidget(self.groupbox, 0, 0)
        mainLayout.addWidget(self.hostLab, 1, 0)
        mainLayout.addLayout(self.butLayout, 2, 0)
        mainLayout.setMargin(10)
        self.setLayout(mainLayout)

        self.reactor = reactor
        self.running = False

        QObject.connect(self.startStopBut, SIGNAL('clicked( )'), self.onStartStop)
        QObject.connect(self.quitBut, SIGNAL('clicked( )'), self.quit)

    def getIP(self):
        s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        s.connect(('www.google.com', 80))
        addr = s.getsockname()[0]
        s.close()
        return addr

    def setupUI(self):
        QtGui.QMainWindow.__init__(self, widget, QtCore.Qt.WindowStaysOnTopHint)
        self.resize(300, 200)
        self.setWindowTitle('Modulo de Seguranca')
        # self.setWindowOpacity(0.1)
        # self.setWindowFlags(QtCore.Qt.FramelessWindowHint)
        # self.setAttribute(QtCore.Qt.WA_NoSystemBackground)
        # self.setWindowFlags(Qt.Window)
        # self.setWindowState(QtCore.Qt.WindowFullScreen)
        # self.setWindowState(QtCore.Qt.WindowStaysOnTopHint)
        # self.setAttribute(QtCore.Qt.WA_PaintOnScreen, True)

        # self.setAttribute(QtCore.Qt.WA_NativeWindow, True)

        # disables qt double buffering (seems X11 only since qt4.5, ...)
        # self.setAttribute(QtCore.Qt.WA_PaintOnScreen, False)

        # self.setAttribute(QtCore.Qt.WA_NoSystemBackground, True)
        # self.setAutoFillBackground(False)

        # Setting style
        QApplication.setStyle(QStyleFactory.create('cleanlooks'))
        QApplication.setPalette(QApplication.style().standardPalette())
        self.setStyleSheet(open(os.path.dirname(__file__) + 'styleSheet.qss', 'r').read())

        # Label
        self.hostLab = QLabel('')

        self.groupbox = QGroupBox()
        formLayout = QFormLayout()

        # LineEdit
        self.portEdit = QLineEdit()
        self.portEdit.setText('5000')

        self.addrEdit = QLineEdit()
        self.addrEdit.setText(self.getIP())
        self.addrEdit.setEnabled(False)

        self.passwdEdit = QLineEdit()

        formLayout.addRow(QLabel('Address'), self.addrEdit)
        formLayout.addRow(QLabel('Port'), self.portEdit)
        formLayout.addRow(QLabel('Password'), self.passwdEdit)

        self.groupbox.setLayout(formLayout)

        # Create Button
        self.butLayout = QHBoxLayout()
        self.startStopBut = QPushButton('Start')
        self.quitBut = QPushButton('Quit')
        self.butLayout.addWidget(self.startStopBut)
        self.butLayout.addWidget(self.quitBut)

    def onStartStop(self):
        if not self.running:
            self._start()
        else:
            self._stop()

    def _start(self):
        port = int(self.portEdit.text())
        pwd = str(self.passwdEdit.text())
        self.startStopBut.setText('Close')
        self.reactor.listenTCP(port, RDCFactory(password=pwd))
        self.running = True

    def _stop(self):
        self.startStopBut.setText('Start')
        self.reactor.stop()
        self.running = False

    def quit(self):
        # call reactor of stop method
        self.reactor.stop()
        # call QDialog of method to close the gui window
        self.close()

    def closeEvent(self, event):
        self.quit()


if __name__ == '__main__':
    from twisted.internet import reactor
    rdcServerGUI = RDCServerGUI(reactor)
    rdcServerGUI.show()

    port = 5000
    pwd = '1234'
    reactor.listenTCP(port, RDCFactory(password=pwd))
    reactor.run()
